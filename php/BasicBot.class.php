<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

class BasicBot {
	protected $sock, $debug, $fp;
	public $track_data, $tracks;
	public $carlength, $carwidth, $carguide;
	public $carspeed, $carpiece, $cardist, $carangle;
	public $turbo, $throttle;
	
	function __construct($host, $port, $botname, $botkey, $debug = FALSE) {
		$this->debug = $debug;
		$this->connect($host, $port, $botkey);

			//$course = 'germany';	// Germany
			//$course = 'usa';		// U.S.A
			$course = 'keimola';	// Finland
			//$course = 'france';		// France
		/*
			$this->write_msg('createRace', array(
				'botId' => array(
					'name' => $botname,
					'key' => $botkey),
				'trackName' => $course,
				'password' => 'GLS123',
				'carCount' => 1,
			));

			$this->logfile = date('YmdHis').'.log';
			$this->fp = fopen('/Users/ksan/Log/'.$this->logfile, 'a+');
*/


		$this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));

		
		$this->carspeed = 0;
		$this->carpiece = 0;
		$this->cardist = 0;
		$this->turbo = 0;
		$this->throttle = 1.0;
		$this->carangle = 0;
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		}
		//fclose($this->fp);
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}
	
	public function run() {
		$endflg = false;
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {
				case 'carPositions':
					//$this->writelog($msg);
					$this->throttle_control($msg);
					//$this->write_msg('throttle', 0.5);
					break;
				case 'join':
					//$this->writelog($msg);
					break;
				case 'yourCar':
					//$this->writelog($msg);
					break;
				case 'gameInit':
					$this->course_control($msg);
					//$this->writelog($msg);
					break;
				case 'gameStart':
					//$this->writelog($msg);
					break;
				case 'crash':
					//$this->writelog($msg);
					break;
				case 'spawn':
					//$this->writelog($msg);
					break;
				case 'lapFinished':
					//$this->writelog($msg);
					$endflg = true;
					break;
				case 'dnf':
					//$this->writelog($msg);
					$endflg = true;
					break;
				case 'finish':
					//$this->writelog($msg);
					$endflg = true;
					break;
				case 'turboAvailable':
					//$this->writelog($msg);
					$this->turbo = 1;
					break;
				default:
					$this->write_msg('ping', null);
					break;
			}
			
			if ($endflg == true){
				break;
			}
		}
	}
	
	public function throttle_control($msg){
		$flg = 0;
		$angle = $msg['data'][0]['angle'];
		$piece = $msg['data'][0]['piecePosition']['pieceIndex'];
		$distance = $msg['data'][0]['piecePosition']['inPieceDistance'];
		//$lane = $msg['data'][0]['piecePosition']['inPieceDistance']['lane']['startLaneIndex'];
		
		// Turbo
		if ($this->turbo == 1 && count($this->track_data) - 5 == $piece && $this->tracks[$piece] == 1 && $flg == 0){
			$this->write_msg('turbo', 'Super Turbo!');
			$this->turbo = 0;
			$flg = 1;
			//return;
		}
		

		// Lane change
		if ($this->track_data[$piece]['switch'] == 1 && $flg == 0){
			if ((int)$this->track_data[$piece]['angle'] == 0){
				if ($this->track_data[$piece+1]['angle'] > 0){
					$this->write_msg('switchLane', 'Left');
				}else if ($this->track_data[$piece+1]['angle'] < 0){
					$this->write_msg('switchLane', 'Right');
				}
			}else if ($this->track_data[$piece]['angle'] > 0){
					$this->write_msg('switchLane', 'Left');
			}else if ($this->track_data[$piece]['angle'] < 0){
					$this->write_msg('switchLane', 'Right');
			}
		}

		
		
		// Check Speed
		if ($this->carspeed == 0 && $this->cardist == 0 && $this->carpiece == 0){
			$this->carspeed = 0.1;
			$this->carpiece = $piece;
			$this->cardist = $distance;
		}else{
			if ($this->carpiece != $piece){
				if ($piece - 1 < 0){
					$pic = count($this->track_data) - 1;
				}else{
					$pic = $piece - 1;
				}
				
				if ($distance - ($this->track_data[$pic]['length'] - $this->cardist) < 20)
					$this->carspeed = $distance - ($this->track_data[$pic]['length'] - $this->cardist);
			}else{
				if ($distance - $this->cardist < 20)
					$this->carspeed = $distance - $this->cardist;
			}
			$this->carpiece = $piece;
			$this->cardist = $distance;
		}
				
		$next = $piece + 1;
		$next2 = $piece + 2;
		if ($next == count($this->tracks)){
			$next = 0;
		}
		if ($next2 == count($this->tracks)+1){
			$next2 = 1;
		}
		
		if ($this->carspeed > 5){
			$thro = 0.5;
		}else if ($this->carspeed > 3.0){
			$thro = 0.7;
		}else{
			$thro = 1.0;
		}
		
		$throflg = 0;
					
		if ($this->tracks[$piece] == 1 || $this->tracks[$piece] == 3){
			$pos = abs(($this->track_data[$piece]['length'] - $distance) / 100);
			$pos = $pos * 1.2;
			if ($pos > 1)
				$pos = 1;
			//if ($this->tracks[$next] == 2 && $pos < $this->carlength){
			if (abs($angle) > 30){
				 $this->throttle = 0.0;
				 $throflg = 1;
			}else{
				if ($this->tracks[$next] == 1 && $this->tracks[$next2] == 2 && abs($this->track_data[$next2]['angle']) >= 30){
					if ($this->carspeed > 5.0){
						//$this->throttle = $pos * 1.5;
						$this->throttle = 0.5;
						$throflg = 2;
					}else{
						$this->throttle = 0.6;
						$throflg = 3;
					}
				}else if ($this->tracks[$next] == 3 && $this->tracks[$next2] == 2 && abs($this->track_data[$next2]['angle']) >= 30){
					if ($this->carspeed > 4.0){
						$this->throttle = $pos * 1.5;
						$throflg = 4;
					}else{
						$this->throttle = 0.6;
						$throflg = 5;
					}
				}else if ($this->tracks[$next] == 1 && $this->tracks[$next2] == 4 && abs($this->track_data[$next2]['angle']) >= 30){
					if ($this->carspeed > 5.0){
						$this->throttle = $pos * 1.2;
						$throflg = 6;
					}else{
						$this->throttle = 0.4;
						$throflg = 7;
					}
				}else if ($this->tracks[$next] == 3 && $this->tracks[$next2] == 4 && abs($this->track_data[$next2]['angle']) >= 30){
					if ($this->carspeed > 4.0){
						$this->throttle = $pos * 1.2;
						$throflg = 8;
					}else{
						$this->throttle = 0.3;
						$throflg = 9;
					}
				}else if ($this->tracks[$next] == 2){
					if ($this->carspeed > 5.0){
						$this->throttle = 0.0;
						$throflg = 10;
					}else if ($this->carspeed > 3.0){
						$this->throttle = $pos;
						//$this->throttle = 0;
						$throflg = 11;
					}else{
						$this->throttle = 0.3;
						$throflg = 12;
					}
				}else if ($this->tracks[$next] == 3){
					if ($this->carspeed > 5.0){
						$this->throttle = $pos;
						//$this->throttle = 0.1;
						$throflg = 13;
					}else{
						$this->throttle = 0.7;
						$throflg = 14;
					}
				}else if ($this->tracks[$next] == 4){
					if ($this->carspeed > 5.0){
						$this->throttle = 0.0;
						$throflg = 15;
					}else if ($this->carspeed > 2.0){
						$this->throttle = $pos;
						//$this->throttle = 0.1;
						$throflg = 16;
					}else{
						$this->throttle = 0.5;
						$throflg = 17;
					}
				}else{
					$this->throttle = 1.0;
					$throflg = 18;
				}
			}
		}else{
			if ($this->tracks[$next] == 4){
				if (abs($angle) > 10){
					$this->throttle = 0.0;
					$throflg = 19;
				}else{
					$this->throttle = 0.5;
					$throflg = 20;
				}
			}else{
				if ($this->tracks[$next] == 1){
					$this->throttle = 0.7;
					$throflg = 21;
				}else if (abs($angle) > 30){
						$this->throttle = 0.0;	
						$throflg = 22;
				}else if (abs($angle) > 20){
						$this->throttle = 0.1 * $thro;
						$throflg = 23;
				}else if (abs($angle) > 15){
						$this->throttle = 0.3 * $thro;
						$throflg = 24;
				}else if (abs($angle) > 10){
					if (abs($angle) < $this->carangle){
						$this->throttle = 0.6;
						$throflg = 25;
					}else{
						$this->throttle = 0.6 * $thro;
						$throflg = 26;
					}
				}else if (abs($angle) > 5){
					if (abs($angle) < $this->carangle){
						$this->throttle = 0.8;
						$throflg = 27;
					}else{
						$this->throttle = 0.8 * $thro;
						$throflg = 28;
					}
				}else{
					if (abs($angle) == 0){
						$this->throttle = 1.0;
						$throflg = 29;
					}else{
						$this->throttle = 1.0 * $thro;
						$throflg = 30;
					}
				}
			}
		}
		
			//$buff = sprintf('Speed : %f  Throtte : %f  Angle : %f  Next : %d  ThroPos : %d', $this->carspeed, $this->throttle, $angle, $this->tracks[$next], $throflg);
			//fwrite($this->fp, $buff."\r\n");
		
		$this->carangle = abs($angle);
		
		$this->write_msg('throttle', $this->throttle);
	}
	
	public function course_control($msg){
		$this->track_data = $msg['data']['race']['track']['pieces'];
		$track_length = count($this->track_data);
		$this->tracks = array();
		for ($i = 0; $i < $track_length; $i++){
			if ((int)$this->track_data[$i]['length'] > 0){
				$this->tracks[$i] = 1;
			}else{
				$angle = abs($this->track_data[$i]['angle']);
				$radius = abs($this->track_data[$i]['radius']);
				if ($radius / $angle > 4){
					$this->tracks[$i] = 3;
				}else if ($radius / $angle < 2){
					$this->tracks[$i] = 4;
				}else{
					$this->tracks[$i] = 2;
				}
			}
		} 
		
		$this->carlength = $msg['data']['race']['car'][0]['dimensions']['length'];
		$this->carwidth = $msg['data']['race']['car'][0]['dimensions']['width'];
		$this->carguide = $msg['data']['race']['car'][0]['dimensions']['guideFlagPosition'];
		//$this->writelog($this->tracks);
	}
	
	public function writelog($msg){
		/*
			ob_start();

			print_r($msg);

			$buffer = ob_get_contents();
			ob_end_clean();

		
			fwrite($this->fp, $buffer);
		*/
	}
}
?>